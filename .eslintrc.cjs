'use strict';

module.exports = {
  root: true,
  ignorePatterns: [
    '**/node_modules/*',
    '**/coverage/*',
    '**/dist/*',
    'projects/frontend/**/*.prod.ts',
    'projects/frontend/**/polyfills.ts',
    'projects/openapi/**/openapi.d.ts',
  ],
  overrides: [
    {
      files: ['*.ts'],
      parserOptions: { project: [] },
      extends: [
        'plugin:@typescript-eslint/recommended',
        'airbnb-typescript/base',
        'plugin:@angular-eslint/recommended',
        'plugin:prettier/recommended',
      ],
      plugins: ['deprecation'],
      rules: {
        '@typescript-eslint/array-type': ['error', { default: 'array' }],
        '@typescript-eslint/ban-types': 'off',
        '@typescript-eslint/lines-between-class-members': ['error', 'always', { exceptAfterSingleLine: true }],
        '@typescript-eslint/naming-convention': [
          'error',
          { selector: 'variable', format: ['camelCase', 'PascalCase', 'snake_case', 'UPPER_CASE'] },
        ],
        '@typescript-eslint/no-empty-function': ['error', { allow: ['arrowFunctions', 'constructors'] }],
        '@typescript-eslint/no-explicit-any': 'error',
        '@typescript-eslint/no-inferrable-types': 'off',
        '@typescript-eslint/no-non-null-assertion': 'error',
        '@typescript-eslint/no-use-before-define': ['error', { functions: false }],
        '@typescript-eslint/no-useless-constructor': 'off',
        '@typescript-eslint/no-var-requires': 'error',
        '@typescript-eslint/typedef': [
          'error',
          { arrowParameter: true, memberVariableDeclaration: true, parameter: true, variableDeclaration: true },
        ],
        'deprecation/deprecation': 'warn',
      },
    },
    {
      files: ['*.js', '*.cjs'],
      parserOptions: { ecmaVersion: 2020 },
      env: { es6: true },
      plugins: ['html'],
      extends: ['airbnb-base/legacy', 'plugin:prettier/recommended'],
    },
    {
      files: ['*.js'],
      parserOptions: { sourceType: 'module' },
    },
    {
      files: ['*.js', '*.ts', '*.cjs'],
      parserOptions: { project: [] },
      rules: {
        'class-methods-use-this': 'off',
        'consistent-return': 'off',
        'import/prefer-default-export': 'off',
        'max-classes-per-file': ['error', 1],
        'max-len': ['error', { code: 140, ignoreStrings: true, ignoreRegExpLiterals: true, ignoreTemplateLiterals: true }],
        'no-await-in-loop': 'off',
        'no-console': ['error', { allow: ['error', 'log', 'warn'] }],
        'no-empty': 'error',
        'no-multiple-empty-lines': ['error', { max: 1 }],
        'no-param-reassign': ['error', { props: false }],
        'no-return-assign': 'off',
        'no-underscore-dangle': ['error', { allow: ['__dirname'], allowAfterThis: true }],
        'padding-line-between-statements': [
          'error',
          { blankLine: 'always', prev: '*', next: 'return' },
          { blankLine: 'always', prev: ['const', 'let'], next: '*' },
          { blankLine: 'any', prev: ['const', 'let'], next: ['const', 'let'] },
        ],
        'quote-props': ['error', 'as-needed'],
        quotes: ['error', 'single'],
        'sort-keys': 'off',
      },
    },
    {
      files: ['projects/openapi/**/*.ts'],
      parserOptions: {
        project: ['projects/openapi/tsconfig.json'],
      },
    },
    {
      files: ['projects/frontend/**/*.ts'],
      parserOptions: {
        project: ['projects/frontend/tsconfig.app.json', 'projects/frontend/tsconfig.spec.json'],
      },
    },
    {
      files: ['projects/backend/**/*.ts'],
      parserOptions: {
        project: ['projects/backend/tsconfig.json'],
      },
    },
    {
      files: ['*.component.html'],
      extends: ['plugin:@angular-eslint/template/recommended'],
      rules: {},
    },
    {
      files: ['*.component.ts'],
      extends: ['plugin:@angular-eslint/template/process-inline-templates'],
    },
  ],
};

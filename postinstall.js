/**
 * This node script installs dependencies of a project, excluding devDependecies if NODE_ENV is 'production'
 * @param projectfolder - optional path to project folder (e.g.: projects/backend). If omitted, all projects will be installed
 * If project has peer dependencies, npm install tries to find them in project's node_modules
 * If npm install cannot find there a peer dependency, a warning is thrown to install it yourself
 * This script makes this warning ignored if peer dependency is already installed to project root's node_modules, and versions are met
 */

import { spawn } from 'child_process';
import { promises as fs } from 'fs';
import semver from 'semver';

(async () => {
  try {
    const { argv = [], env = {} } = process;
    const isProd = env.NODE_ENV === 'production';
    let projectPaths = Array.isArray(argv) ? argv.slice(2, argv.length) : [];

    if (projectPaths.length === 0) {
      projectPaths = ((await fs.readdir('./projects', 'utf-8')) || []).map(project => `projects/${project}`);
    }

    const isInstalledInProjectRoot = async peerPackage => {
      try {
        const moduleName = peerPackage.substring(0, peerPackage.lastIndexOf('@'));
        const peerVersion = peerPackage.substring(peerPackage.lastIndexOf('@') + 1, peerPackage.length);
        const { version } = JSON.parse(await fs.readFile(`./node_modules/${moduleName}/package.json`, 'utf-8'));

        return semver.satisfies(version, peerVersion);
      } catch (err) {
        if (err && err.code === 'ENOENT') {
          return false;
        }
        console.error(err);

        return false;
      }
    };

    const install = projectPath => {
      return new Promise((resolve, reject) => {
        try {
          console.log(`Installing following project: ${projectPath} ...`);
          const child = spawn(`cd ${projectPath} && npm ci ${isProd ? '--only=production' : ''}`, {
            shell: true,
            stdio: ['inherit', 'inherit', 'pipe'],
          });
          let chunk = '';

          child.on('error', err => reject(err));
          child.on('exit', resolve);
          child.stderr.on('data', async data => {
            chunk += data.toString();
            if (!chunk.endsWith('\n')) return;
            let lines = chunk.split('\n');

            const hideLines = await Promise.all(
              lines.map(line => {
                if (!line.includes('but none is installed. You must install peer dependencies yourself.')) {
                  return Promise.resolve(false);
                }
                const [, peerPackage] = line.split(' ').filter(word => word.includes('@'));

                return isInstalledInProjectRoot(peerPackage);
              }),
            );

            lines = lines.filter((_line, index) => hideLines[index] === false);
            process.stderr.write(lines.join('\n'));
            chunk = '';
          });
        } catch (err) {
          reject(err);
        }
      });
    };

    // eslint-disable-next-line no-restricted-syntax
    for (let projectPath of projectPaths) {
      await install(projectPath, isProd);
    }
  } catch (err) {
    console.error(err);
  }
})();

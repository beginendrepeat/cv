import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { Provider } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { CvResponse } from 'contract';
import { AppComponent } from './app.component';
import { HttpService } from './services/http-service/http.service';

const httpServiceMock: { handleError: jest.Mock } = { handleError: jest.fn() };
const providers: Provider = [AppComponent, { provide: HttpService, useValue: httpServiceMock }];

describe('AppComponent', () => {
  let appComponent: AppComponent;
  let httpMock: HttpTestingController;

  beforeAll(() => {
    TestBed.configureTestingModule({ imports: [HttpClientTestingModule], providers });
    appComponent = TestBed.inject(AppComponent);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterAll(() => {
    appComponent = null;
    httpMock = null;
  });

  it('should initialize', () => {
    expect(appComponent).toBeDefined();
  });

  describe('cv$', () => {
    it('should fetch content', (done: jest.DoneCallback) => {
      const successResponse: CvResponse = { title: 'cv' };

      appComponent.cv$.subscribe((cv: CvResponse) => {
        expect(cv).toStrictEqual(successResponse);
        done();
      });

      const req: TestRequest = httpMock.expectOne('/cv');

      expect(req.request.method).toBe('GET');
      req.flush(successResponse);

      httpMock.verify();
    });
  });
});

import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  constructor() {}

  /**
   * Public - Handle Http operation that failed, let the app continue.
   * @param err - error object catched by the rxjs catchError operator
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   * @returns default result object
   */
  handleError<AnyType>(err: unknown, operation: string = 'operation', result?: AnyType): Observable<AnyType> {
    console.log(`Error while ${operation}`);
    console.error(err);

    return of<AnyType>(result);
  }
}

import { Subscription } from 'rxjs';
import { HttpService } from './http.service';

describe('HttpService()', () => {
  let httpService: HttpService = new HttpService();

  it('should initialize', (): void => {
    expect(httpService).toBeDefined();
  });

  describe('handleError', () => {
    beforeAll(() => {
      httpService = new HttpService();
    });

    let subscription: Subscription = null;

    it('should return default result', (done: () => void): void => {
      type MockType = string;
      const defaultResult: MockType = 'MockResult';
      const consoleLog: jest.SpyInstance = jest.spyOn(console, 'log').mockImplementation(() => {});
      const consoleError: jest.SpyInstance = jest.spyOn(console, 'error').mockImplementation(() => {});
      const err: unknown = {
        status: 500,
        detail: 'MockDetail',
      };
      const mockOperation: string = 'mock_operation';

      subscription = httpService.handleError<MockType>(err, mockOperation, defaultResult).subscribe((result: MockType) => {
        expect(result).toBe(defaultResult);
        expect(consoleLog).toBeCalledTimes(1);
        expect(consoleLog).toBeCalledWith(`Error while ${mockOperation}`);
        expect(consoleError).toBeCalledTimes(1);
        expect(consoleError).toBeCalledWith(err);
        done();
      });
    });

    afterAll(() => {
      if (subscription) {
        subscription.unsubscribe();
      }
    });
  });
});

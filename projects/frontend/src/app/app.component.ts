import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CvResponse } from 'contract';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { HttpService } from './services/http-service/http.service';

@Component({
  selector: 'body',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  cv$: Observable<CvResponse> = this.http
    .get('/cv')
    .pipe(catchError((err: unknown) => this.httpService.handleError<CvResponse>(err, 'getCv:', null)));

  constructor(private http: HttpClient, private httpService: HttpService) {}
}

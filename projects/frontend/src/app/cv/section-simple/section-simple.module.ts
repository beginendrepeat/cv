import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SectionSimpleComponent } from './section-simple.component';
import { TitleModule } from '../title/title.module';

@NgModule({
  declarations: [SectionSimpleComponent],
  imports: [CommonModule, TitleModule],
  exports: [SectionSimpleComponent],
})
export class SectionSimpleModule {}

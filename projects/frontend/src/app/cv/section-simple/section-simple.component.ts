import { Component, Input } from '@angular/core';
import { DataSimple, SectionSimple } from 'contract';

@Component({
  selector: 'app-section-simple',
  templateUrl: './section-simple.component.html',
  styleUrls: ['./section-simple.component.scss'],
})
export class SectionSimpleComponent {
  @Input() sectionSimple: SectionSimple;

  trackByFn(_index: number, row: DataSimple): string {
    return row.title || row.value;
  }
}

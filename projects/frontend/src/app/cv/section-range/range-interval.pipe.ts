import { Pipe, PipeTransform } from '@angular/core';
import { DataRange } from 'contract';

@Pipe({
  name: 'rangeInterval',
})
export class RangeIntervalPipe implements PipeTransform {
  constructor() {}

  transform(dataRange: DataRange): string {
    return [dataRange?.start, dataRange?.end].filter(Boolean).join(' \u2192 ');
  }
}

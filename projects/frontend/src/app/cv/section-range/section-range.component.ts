import { Component, Input } from '@angular/core';
import { DataRange, SectionRange } from 'contract';

@Component({
  selector: 'app-section-range',
  templateUrl: './section-range.component.html',
  styleUrls: ['./section-range.component.scss'],
})
export class SectionRangeComponent {
  @Input() sectionRange: SectionRange;

  trackByFn(_index: number, row: DataRange): string {
    return row.end;
  }

  trackByFnActivity(_index: number, row: string): string {
    return row;
  }
}

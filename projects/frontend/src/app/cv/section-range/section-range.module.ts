import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { RangeIntervalPipe } from './range-interval.pipe';
import { SectionRangeComponent } from './section-range.component';
import { TitleModule } from '../title/title.module';

@NgModule({
  declarations: [RangeIntervalPipe, SectionRangeComponent],
  imports: [CommonModule, TitleModule],
  exports: [RangeIntervalPipe, SectionRangeComponent],
})
export class SectionRangeModule {}

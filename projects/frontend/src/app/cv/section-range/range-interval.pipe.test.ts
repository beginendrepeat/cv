import { RangeIntervalPipe } from './range-interval.pipe';

describe('RangeIntervalPipe', () => {
  const rangeIntervalPipe: RangeIntervalPipe = new RangeIntervalPipe();

  it('should be initialized', () => {
    expect(rangeIntervalPipe).toBeDefined();
  });

  describe('transform()', () => {
    interface TestCase {
      start: string;
      end: string;
      result: string;
    }

    it.each`
      start        | end       | result
      ${'2000'}    | ${'2010'} | ${'2000 \u2192 2010'}
      ${'2000'}    | ${''}     | ${'2000'}
      ${null}      | ${'2010'} | ${'2010'}
      ${null}      | ${''}     | ${''}
      ${undefined} | ${''}     | ${''}
      ${NaN}       | ${''}     | ${''}
    `('should return for start: $start and send: $end this result: $result', ({ start, end, result }: TestCase): void => {
      expect(rangeIntervalPipe.transform({ start, end })).toBe(result);
    });
  });
});

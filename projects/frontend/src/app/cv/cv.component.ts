import { Component, Input } from '@angular/core';
import { CvResponse, Section } from 'contract';

@Component({
  selector: 'app-cv',
  templateUrl: './cv.component.html',
  styleUrls: ['./cv.component.scss'],
})
export class CvComponent {
  @Input() cv: CvResponse;

  footer: string = `Budapest, ${new Date().toISOString().split('T')[0]}`;

  trackByFn(_index: number, section: Section): string {
    return section?.title;
  }
}

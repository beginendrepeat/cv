import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SectionHeaderComponent } from './section-header.component';
import { TitleModule } from '../title/title.module';

@NgModule({
  declarations: [SectionHeaderComponent],
  imports: [CommonModule, TitleModule],
  exports: [SectionHeaderComponent],
})
export class SectionHeaderModule {}

import { Component, Input } from '@angular/core';
import { DataSimple, SectionHeader } from 'contract';

@Component({
  selector: 'app-section-header',
  templateUrl: './section-header.component.html',
  styleUrls: ['./section-header.component.scss'],
})
export class SectionHeaderComponent {
  @Input() sectionHeader: SectionHeader;

  trackByFn(_index: number, row: DataSimple): string {
    return row.title;
  }
}

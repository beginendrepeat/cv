import { Component, Input } from '@angular/core';
import { SectionSkills } from 'contract';

@Component({
  selector: 'app-section-skills',
  templateUrl: './section-skills.component.html',
  styleUrls: ['./section-skills.component.scss'],
})
export class SectionSkillsComponent {
  @Input() sectionSkills: SectionSkills;

  trackByFn(_index: number, skill: string): string {
    return skill;
  }
}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SectionSkillsComponent } from './section-skills.component';
import { TitleModule } from '../title/title.module';

@NgModule({
  declarations: [SectionSkillsComponent],
  imports: [CommonModule, TitleModule],
  exports: [SectionSkillsComponent],
})
export class SectionSkillsModule {}

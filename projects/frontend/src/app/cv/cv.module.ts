import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CvComponent } from './cv.component';
import { SectionHeaderModule } from './section-header/section-header.module';
import { SectionRangeModule } from './section-range/section-range.module';
import { SectionSimpleModule } from './section-simple/section-simple.module';
import { SectionSkillsModule } from './section-skills/section-skills.module';

@NgModule({
  declarations: [CvComponent],
  imports: [CommonModule, SectionHeaderModule, SectionRangeModule, SectionSimpleModule, SectionSkillsModule],
  exports: [CvComponent],
})
export class CvModule {}

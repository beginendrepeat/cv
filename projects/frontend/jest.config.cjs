'use strict';

const esModulesToTransform = [].join('|');

module.exports = {
  verbose: true,
  moduleNameMapper: {
    contract: '<rootDir>/projects/openapi/contract/openapi.d.ts',
  },
  modulePathIgnorePatterns: [
    '<rootDir>/projects/backend/',
    '<rootDir>/projects/contract/',
    '<rootDir>/projects/frontend/dist/',
    '<rootDir>/projects/frontend/src/environments/',
  ],
  coverageThreshold: {
    global: {
      branches: 60,
    },
  },
  transformIgnorePatterns: [
    `node_modules/(?!${esModulesToTransform}).+/.+.js$`, // need to transfrom these to commonjs
  ],
};

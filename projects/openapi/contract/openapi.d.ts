export interface AvatarField {
  /**
   * Url to a photo image
   */
  avatar?: string;
}
export interface CvResponse {
  /**
   * title
   */
  title?: string;
  sections?: Section[];
}
/**
 * Time range activities
 */
export interface DataRange {
  start?: string | null;
  end?: string | null;
  activities?: string[];
}
/**
 * Simple key-value string pair for a cv row
 */
export interface DataSimple {
  /**
   * title
   */
  title?: string;
  value?: string;
}
export namespace Get {
  namespace Responses {
    export type $200 = SetLogResponse;
  }
}
export namespace Level {
  export type Level = "error" | "warn" | "info" | "http" | "verbose" | "debug" | "silly";
}
export type Section = SectionHeader | SectionRange | SectionSimple | SectionSkills;
export interface SectionHeader {
  /**
   * Url to a photo image
   */
  avatar?: string;
  /**
   * title
   */
  title?: string;
  type?: "section_header";
  rows?: DataSimple[];
}
export interface SectionRange {
  /**
   * title
   */
  title?: string;
  type?: "section_range";
  rows?: DataRange[];
}
export interface SectionSimple {
  /**
   * title
   */
  title?: string;
  type?: "section_simple";
  rows?: DataSimple[];
}
export interface SectionSkills {
  /**
   * title
   */
  title?: string;
  type?: "section_skills";
  skills?: string[];
}
/**
 * Set-log response.
 */
export interface SetLogResponse {
  /**
   * example:
   * Log level changed: info -> debug
   */
  message?: string;
}
export interface TitleField {
  /**
   * title
   */
  title?: string;
}

/* Loads openapi.yaml file
 * In order to include error responses too:
 *   - responses are moved to schemas
 *   - and also referenced from there
 */
import * as fs from 'fs';
import * as jsYaml from 'js-yaml';
import dtsGenerator from 'dtsgenerator';
import { Contract, Obj } from './contract';

(async () => {
  try {
    const yaml: string = fs
      .readFileSync('./contract/openapi.yaml', 'utf-8')
      .toString()
      .replace(/#\/components\/responses/g, '#/components/schemas');

    const contract: Contract = jsYaml.load(yaml) as Contract;

    Object.assign(contract.components.schemas, contract.components.responses);

    const removeRepeatedFieldsFromAllOf: (schema: object) => void = (schema: object) => {
      /**
       * If contract contains object property name repeated twice in an anyOf array,
       * one usually contains the main object skeleton, and the second slightly modifies it in Swagger UI.
       * However, dtsgenerator unfortunately overwrites whichever is the first with the second.
       * This function deletes the customly added repeated one, and keeps in the main object skeleton,
       * which is usually enough to generate a proper typescript interface file.
       */
      const repeatedFields: string[] = ['headline'];

      Object.keys(schema).forEach((prop: string) => {
        const obj: Obj = schema[prop];

        if (obj && obj.allOf) {
          obj.allOf.forEach((item: Obj) => {
            if (item && item.properties) {
              repeatedFields.forEach((field: string) => delete item.properties[field]);
            }
          });
        }
      });
    };

    removeRepeatedFieldsFromAllOf(contract.components.schemas);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    const output: string = (await dtsGenerator?.default({ contents: [contract], namespaceName: '' }))
      .split('\n')
      .map((line: string) => (line.startsWith('declare') ? line.replace('declare', 'export') : line))
      .map((line: string) => line.replace(/[\s]{4}/g, '  '))
      .join('\n');

    fs.writeFileSync('./contract/openapi.d.ts', output, 'utf-8');

    console.log('openapi.yaml is successfully converted to openapi.d.ts');
  } catch (err) {
    console.error(err);
  }
})();

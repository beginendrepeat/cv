export interface Contract {
  openapi: string;
  info: {
    title: string;
    description: string;
    version: string;
  };
  paths: object;
  components: {
    parameters: object;
    responses: object;
    schemas: object;
    securitySchemes: {
      BasicAuth: {
        schema: string;
        type: string;
      };
    };
  };
}

export interface Obj {
  allOf?: Obj[];
  properties: object;
}

import winston, { Logform, Logger } from 'winston';
import { isProd } from './config';

const { format } = winston;
const { colorize, combine, timestamp, printf } = format;

export const logger: Logger = winston.createLogger({
  level: isProd ? 'info' : 'debug',
  format: combine(
    colorize(),
    timestamp(),
    printf((log: Logform.TransformableInfo) => `${log.timestamp} ${log.level}: ${log.message}`),
  ),
  transports: [new winston.transports.Console()],
});

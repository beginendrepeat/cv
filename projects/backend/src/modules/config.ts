const { env } = process;

export const port: number = 8080;
export const isProd: boolean = env.NODE_ENV === 'production';

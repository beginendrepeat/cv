import express, { Express, NextFunction, Request, Response } from 'express';
import os from 'os';
import { dirname, join } from 'path';
import { fileURLToPath } from 'url';

import { appRoutes } from './routes';
import { logger } from './modules/logger';
import { port } from './modules/config';

const actualFolder: string = dirname(fileURLToPath(import.meta.url));

(async (app: Express) => {
  try {
    const { env = {} } = process;

    logger.info(`NodeJS v${process.versions.node}, Environment: ${env.NODE_ENV}, Application: ${env.npm_package_version}`);
    logger.info(`Server cpu cores: ${os.cpus().length}`);

    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));
    app.use((req: Request, _resp: Response, next: NextFunction) => {
      logger.http(`${req.method} ${req.originalUrl}`);
      next();
    });
    app.use('/public', express.static(join(actualFolder, '..', 'public')));
    appRoutes(app);

    app.listen(port, () => logger.info(`Server listening on port ${port}`));
  } catch (err: unknown) {
    logger.error(err);
  }
})(express());

import { Express, NextFunction, Request, Response } from 'express';
import { SetLogResponse } from 'contract';

import { logger } from '../modules/logger';

export function setLog(app: Express, route: string): void {
  app.get(route, (req: Request, resp: Response, next: NextFunction) => {
    try {
      const { query = {} } = req;
      const currentLevel: string = logger.level;

      logger.debug(`Current log level: ${currentLevel}`);
      logger.debug(`Query params: ${JSON.stringify(query)}`);
      const { level } = query;

      logger.debug(`Level in query: ${level}`);

      if (!level) {
        const error: string = `Invalid value for 'level' query parameter: ${level}`;

        logger.warn(error);

        return resp.status(400).json({
          error,
          hint: `valid route: ${route}?level=value, and valid values: error, warn, info, http, verbose, debug, silly`,
        });
      }

      logger.level = String(level);
      const setLogResponse: SetLogResponse = { message: `Log level changed: ${currentLevel} -> ${level}` };

      logger.verbose(`200 - ${JSON.stringify(setLogResponse)}`);

      return resp.status(200).json(setLogResponse);
    } catch (err: unknown) {
      logger.error(err);

      return next(err);
    }
  });
}

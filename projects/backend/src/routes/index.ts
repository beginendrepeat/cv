import { Express } from 'express';

import { cv } from './cv';
import { setLog } from './set-log';

export function appRoutes(app: Express): void {
  cv(app, '/cv');
  setLog(app, '/setlog');
}

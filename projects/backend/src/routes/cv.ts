import { CvResponse } from 'contract';
import { Express, NextFunction, Request, Response } from 'express';
import { promises as fs } from 'fs';
import { dirname, join } from 'path';
import { fileURLToPath } from 'url';

import { logger } from '../modules/logger';

const actualFolder: string = dirname(fileURLToPath(import.meta.url));
const shuffleArray: (array: string[]) => string[] = (array: string[]) => {
  // Durstenfeld shuffle, an optimized version of Fisher-Yates
  for (let i: number = array.length - 1; i > 0; i -= 1) {
    const j: number = Math.floor(Math.random() * (i + 1));

    [array[i], array[j]] = [array[j], array[i]];
  }

  return array;
};

export function cv(app: Express, route: string): void {
  app.get(route, async (_req: Request, resp: Response, next: NextFunction) => {
    try {
      const cvResponse: CvResponse = {
        title: 'Curriculum Vitae',
        sections: [
          {
            type: 'section_header',
            title: 'Personal data',
            avatar: '/public/avatar.jpg',
            rows: [
              { title: 'Name', value: 'Ján<span>os</span> K<!--e-->a<span>to</span>na' },
              { title: 'Date of birth', value: '11<sup>th</sup> October 1985' },
              { title: 'Nationality', value: 'Hungarian' },
              { title: 'Address', value: 'H-1116 Budapest, Kondorosi street' },
              { title: 'Phone number', value: '<span>+</span>36 <!-- 00 --> 30  590 <span>1783</span>' },
              { title: 'E-mail address', value: 'jan<!--a@b.com-->os.ka<!--jan.kana@gmail.com-->tona.cv@gmail.com' },
              { title: 'Source code', value: '<a href="https://gitlab.com/beginendrepeat/cv">https://gitlab.com/beginendrepeat/cv</a>' },
              { title: 'Live preview', value: '<a href="https://appback4appcv.b4a.app/">https://appback4appcv.b4a.app/</a>' },
            ],
          },
          {
            type: 'section_simple',
            title: 'Foreign languages',
            rows: [
              { title: 'English', value: 'Intermediate language exam, type "C", frequent usage during work' },
              { title: 'Russian', value: 'On a basic level' },
            ],
          },
          {
            type: 'section_skills',
            title: 'Skills',
            skills: shuffleArray(await fs.readdir(join(actualFolder, '..', '..', 'public', 'logos'))).map(
              (file: string) => `/public/logos/${file}`,
            ),
          },
          {
            type: 'section_range',
            title: 'Professional skills',
            rows: [
              {
                start: '2019',
                end: 'now',
                activities: [
                  'Lufthansa Systems Hungary, Passenger Experience department',
                  'Roles: Software Developer, Senior, Frontend',
                  'Tasks: develop on-board entertainment and connectivity product',
                  'Web applications using Node.js, Express and Angular 2+',
                  'Typescript, RxJs, Docker, OpenApi, Nginx, Teamcity CI/CD',
                ],
              },
              {
                start: '2008',
                end: '2019',
                activities: [
                  'Budapest Airport, Airport Operations department',
                  'Roles: Traffic analyst trainee, Airport Capacity Planner',
                  'Tasks: process automation, in-house software development',
                  'Web applications using Node.js, Express, AngularJS, MongoDb',
                  'YouTrack REST API, D3.js Gantt charts (check-in, gate, stand)',
                ],
              },
            ],
          },
          {
            type: 'section_range',
            title: 'Studies',
            rows: [
              {
                start: '2004',
                end: '2009',
                activities: [
                  'Student of the Budapest University of Technology and Economics',
                  'Faculty of Transportation, Aeronautical studies',
                  'Control of safety-critical transport processes sub-module',
                  'Qualification: university degree, M. Sc. in Transportation Engineering',
                ],
              },
              { start: null, end: '2003', activities: ['Driving license, category "B"'] },
            ],
          },
          {
            type: 'section_simple',
            title: 'Range of interests',
            rows: [{ title: '', value: ['Hiking', 'cycling', 'snooker', 'windsurfing', 'problem solving games'].join(', ') }],
          },
        ],
      };

      resp.status(200).json(cvResponse);
    } catch (err: unknown) {
      logger.error(err);
      next(err);
    }
  });
}

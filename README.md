# CV

## Live preview without cloning repo
1. Visit [https://appback4appcv.b4a.app/](https://appback4appcv.b4a.app/)

## TL;DR
### Run CV locally in docker
1. Clone source code: `git clone https://gitlab.com/beginendrepeat/cv`
1. Build fronted & backend images and run them with: `docker-compose up -d`
1. Open [http://localhost:8000](https://appback4appcv.b4a.app/)` in browser
1. Remove docker containers: `docker-compose down`

### Run CV locally without docker, development mode
1. Clone source code: `git clone https://gitlab.com/beginendrepeat/cv`
1. Install nodejs 14 on local machine: `nvm use 14.20.1`
1. Install dependencies: `npm ci`
1. Start backend: `npm run start-be`
1. Start frontend: `npm run start-fe`
1. Open [http://localhost:8000](https://appback4appcv.b4a.app/)` in browser

## Detailed features
### Techologies
1. Single repository, multiple projects
1. Typescript is used for both frontend and backend
1. Eslint, prettier style rules are used
1. Stylelint rules are used for scss
1. Jest unit testing framework is used
1. Husky pre-commit hook to ensure code quality in git commits
1. Multi stage build is used in Dockerfiles to reduce size of images

### Backend
1. Express js application
1. GET Endpoint `/cv` provides the CV content as json
1. GET Endpoint `/setlog` can change log level (e.g. `/setlog?level=debug`)
1. Static `public` folder contain static asset files to serve (images, logos)

### Frontend
1. Angular framework
1. It calls relative endpoints, they are proxied to backend
1. Production files run in an nginx docker container, accepting environment variables for route proxy
1. Angular components, service, pipe: passing parameters between parent-child
1. Scss, sass examples: variables, mixins
1. Rxjs observable for http requests, using async pipe for subscribe and unsubscribe
1. Mobile first approach for scss styles 

### Openapi
1. Endpoint definition in an editable `openapi.yaml` file
1. Project converts this `openapi.yaml` file to `openapi.d.ts` file using [dtsgenerator](https://www.npmjs.com/package/dtsgenerator): `npm run contract`
1. Typescript interface file `openapi.d.ts` is referenced in both frontend and backend
